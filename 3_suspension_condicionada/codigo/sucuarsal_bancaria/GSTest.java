public class GSTest {
  public static void main(String[] args) {
    SucursalBancaria sBancaria = new SucursalBancaria();
    new Persona("Persona1", sBancaria);
    new Persona("Persona2", sBancaria);
    new Persona("Persona3", sBancaria);
    new Persona("Persona4", sBancaria);
    new Persona("Persona5", sBancaria);
    new Persona("Persona6", sBancaria);
    new Persona("Persona7", sBancaria);
    new Persona("Persona8", sBancaria);
    new Persona("Persona9", sBancaria);
  }

} class SucursalBancaria {
  //La sucursal bancaria tiene 7 puestos de atención
  public static final int MAX_CAPACITY = 7;
  private int totalAtendidos = 0;

  public synchronized void atender(String member) {
    while (totalAtendidos >= MAX_CAPACITY) {
      try {
        System.out.println(" El banco está lleno " +
                           member + " tiene que esperar ");
        wait();
      } catch (InterruptedException e) {
        //
      }
    }
    //precondition is true
    System.out.println(member + " inicia a ser atendido");
    totalAtendidos = totalAtendidos + 1;
  }
  public synchronized void leave(String member) {
    totalAtendidos = totalAtendidos - 1;
    System.out.println(member +
                       " se ha ido, notificar usuarios en espera");
    notify();
  }
}

class Persona extends Thread {
  private SucursalBancaria sBancaria;
  private String name;

  Persona(String n, SucursalBancaria p) {
    name = n;
    sBancaria =s p;
    start();
  }
  public void run() {
    System.out.println(name + " esta listo para ser atendido");
    sBancaria.atender(name);
    try {
      sleep(750);
    } catch (InterruptedException e) {
      //
    }
    //leave after 500ms
    sBancaria.leave(name);
  }
}
