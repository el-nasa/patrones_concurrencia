# Suspensión condicionada
Es util cuando al invocar un método de un objeto este tiene que 
estar en cierto estado para llevar a cabo la acción, para esto el
patrón sugiere suspender la ejecución del método hasta que la 
precondición se vuelva verdadera.

## Implementación en Java
Todas las clases heredan los métodos *wait, notify y notifyAll* de
la clase *java.lang.Object*. Cuando un hilo invoca el método *wait*:
* Hace que el hilo quite el bloqueo de sincronización del objeto.
* El hilo se mantiene en espera hasta que es notificado usando los
  métodos *notify* o *notifyAll*.
  
## Ejercicio propuesto
Es un sistema simulador de la atención a usuarios de una sucursal
bancaria que cuenta con siete puestos de atención y nueve personas
en fila.

El patrón es utilizado para "simular" el modo en como funcionan las
filas en la vida real, en teoría, todas las personas que se encuentran
en fila se acercan a un punto de atención disponible hasta que no hayan
disponibles, las personas que aún no pasan tienen que esperar hasta 
que uno de los puntos se haya desocupado para dirigirse hacia él.

Análogamente al patrón, la clase que tiene los dos métodos sincronizados
es *SucursalBancaria* la cual tiene un método *antender(String member)*
que como su nombre lo dice atiende al objeto que le sea pasado como
argumento. La precondición que se tiene que cumplir es que el número de
personas atendidas no haya superado el máximo de "cajeros" disponibles,
si esto ocurre la clase persona se pone en espera (el tiempo de atención es
de 750 ms) y luego pasa a ser atendida cuando es notificada con el 
método *notify()*.

El método *alterObjectStateMethod* vendría a ser el método *leave(String member)*
que modifica el estado del objeto para que se cumpla la precondición
y entre una nueva persona a ser atendida.

![modelo funcional](./img/modelo_funcional.png 'modelo funcional')
![modelo estructural](./img/modelo_estructural.png 'modelo estructural')


