# Patrones concurrencia
La computación concurrente consiste en ls multiple ejecución de tareas simultaneas
para resolver cierto problema. La motivación original surge del desarrollo
de técnicas para el uso compartido de usuarios en una misma máquina.

Además, el suo de multiples computadoras es más barato que el de una supercomputadora
para resolver un problema.

En la programación concurrente hay secciones donde se tiene que tener mucho cuidado porque
pueden conducir a resultados no esperados, estas son llamadas *regiones críticas*
y tienen que tener "exclusión mutua" para funcionar.

Los patrones de concurrencia Se encargan de:
* Implementar formas de "bloquear" el código de una clase y ordenar objetos bloqueados para prevenir la ocurrencia de 
carreras (rutinas que compiten por recursos) y *deadlocks* (esperas infinitas).
  
* Los detalles de la optimización del acceso a un recurso de la aplicación 
  que mejore la capacidad de respuesta general de esta.
  
* Los detalles de la ejecución de un método mientras no se cumple 
  una precondición requerida.
  
Los patrones que fueron modelados son:
* Sección crítica (*Critical Section*)
* Orden de bloqueo consistente (*Consistent lock order*)
* Suspensión condicionada (*Guarded Suspension*)
* Bloqueo de lectura/escritura (*Read-Write Lock*)
