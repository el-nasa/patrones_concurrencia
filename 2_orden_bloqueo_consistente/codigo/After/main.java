public class main {
    public static void main(String[] args) {
        //creación de directorios A y B
        Directory objDir_1 = new Directory("Dir1"); //Source
        Directory objDir_2 = new Directory("Dir2"); // Destination

        FileSysUtil_Rev fileSysUtil = new FileSysUtil_Rev();

        //For Thread_A objDir_1 is the source directory
        new MoveContentThr(objDir_1, objDir_2, fileSysUtil);
        //For Thread_B objDir_2 is the source directory
        new MoveContentThr(objDir_2, objDir_1, fileSysUtil);
    }

}

class MoveContentThr extends  Thread{
    private Directory src;
    private Directory dest;
    private FileSysUtil_Rev fSysUtil;

    public MoveContentThr(Directory src, Directory dest, FileSysUtil_Rev fys){
        this.src = src;
        this.dest = dest;
        this.fSysUtil = fys;
        start();
    }
    public void run(){fSysUtil.moveContents(src,dest);}
}
