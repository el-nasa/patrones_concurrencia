# Orden de bloqueo consistente
El patrón de orden de bloqueo consistente (*consistent block order*)
sirve para tratar el problema del *deadlock*, sugieriendo el diseño
de un orden de bloqueo de objetos consistente en toda la aplicación.

## Ejercicio propuesto
Es un simulador del movimiento del contenido de directorios en un 
sistema operativo. En primer lugar, el ejercicio tradicional es un simple
programa que mueve el contenido de un sitio a otro llamando a un método,
pero que puede utilizar técnicas de concurrencia para pasar de forma
simultánea los contenidos de un directorio A a un directorio B y viceversa.

La situación de *deadlock* se presenta normalmente porque al intentar
pasar los contenidos a la vez cada hilo bloquea la clase del directorio
de origen, y como esta espera hasta que la otra clase no tenga un bloqueo,
ambas se quedan esperando infinitamente.

El patrón es implementado al utilizar una función *hashcode* que le asigna
a cada objeto un ID, que es utilizado para definir un orden en 
el que el que tenga el mayor valor de ID será el primero en mover los elementos,
y en caso contrario lo hace el otro, de esta manera ambos realizan la 
operación sin quedarse esperando.

![modelo funcional](./img/modelo_funcional.png 'modelo funcional')
![modelo estructural](./img/modelo_estructural.png 'modelo estructural')