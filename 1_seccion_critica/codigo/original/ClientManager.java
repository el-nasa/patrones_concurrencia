// código modificado por Daniel Alejandro Rodriguez - 20172020009
// Implementación del ejemplo original del libro de Kuchana de sección crítica
// En UDIN no está este ejemplo
//package CriticalSection;
public class ClientManager {

	public static void main(String[] args) {
		// Se toman 50 muestras para comparar los tiempos necesarios
		//for(int i=0;i<50;i++) {
		FileProcess proceso1 = new FileProcess("Thread 1 is wrtting");
		proceso1.start();
		FileProcess proceso2 = new FileProcess("Thread 2 is writting");
		proceso2.start();
		//}
	}
}

class FileProcess extends Thread {
	private String msgLog;

	public FileProcess(String msg) {
		this.msgLog = msg;
	}

	@Override
	public void run() {
		long t1 = System.nanoTime(); // Se toma tiempo inicial para llamar al registrador
		Logger fileLogger = FileLogger.getFileLogger();
		long t2 = System.nanoTime(); // Se toma tiempo final para llamar al registrador
		long timeDiff = t2 - t1; // Se calcula la diferencia entre estos tiempos
		System.out.println(this.msgLog+": t registrador:"+timeDiff); // Se muestra el tiempo tomado en el proceso en milisegundos
		t1 = System.nanoTime(); // Se toma tiempo inicial para escribir 50 mensajes
		for (int i = 0; i < 50; i++) {
			fileLogger.log(msgLog);
		}
		t2 = System.nanoTime(); // Se toma tiempo final para escribir 50 mensajes
		timeDiff = t2 - t1; // Se calcula la diferencia entre estos tiempos
		System.out.println(this.msgLog+": t 50 msg: "+timeDiff); // Se muestra el tiempo tomado en el proceso en milisegundos
	}

}

