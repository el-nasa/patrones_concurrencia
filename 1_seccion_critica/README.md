# Sección crítica
Una sección crítica es un segmento de código que debe ser ejecutado por
un solo hilo a la vez para producir los resultados esperados.

|Sección crítica|Monitor|
|---------|-------|
|Más estricto|Menos estricto|
|Requiere bloquear una clase entera de objetos| Bloquea solo un objeto|
|Implementación en Java
|Usa una combinación de las palabras clave *static* y *synchronized*| Requiere que el método sea declarado con la palabra *synchronized*|
|Dos hilos distintos no pueden ejecutar el código en dos objetos distintos (bloqueo de clase)|Dos hilos no pueden ejecutar el código sincronizado en el mismo objeto, pero si en objetos distintos|

## Ejercicio propuesto
Es un registrador de mensajes a texto plano que es usado por dos hilos que evidencia
la ejecución concurrente de un mismo registrador (*FileLogger*).
La ejemplificación usa el patrón *Singleton* que garantiza que solo
un registrador es creado.
La documentación de Kuchana. P muestra tres acercamientos para lograrlo:
* "Original": Se utiliza el Singleton para ejemplificar al registrador y la palabra reservada *static* para obtenerlo.
* Sección crítica: Se utiliza el Singleton para ejemplificar al registrador y las palabras reservadas *static* y *synchronized* para obtenerlo.
* Inicialización temprana: Se crea el registrador al iniciar la aplicación y se utiliza la palabra reservada *static* para obtenerlo.

![modelo funcional](./img/modelo_funcional.png 'modelo funcional')
![modelo estructural](./img/modelo_estructural.png 'modelo estructural')

### Análisis
El análisis profundo se puede ver en el pdf adjunto.