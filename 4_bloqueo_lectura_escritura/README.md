# Bloqueo de lectura y escritura
Este patrón sirve para mejorar el tiempo de respuesta
de una aplicación que lea y escriba valores en un archivo.
Se basa en la idea de que bloquear una sección de cuando
un hilo solo está leyendo valores vuelve lenta una aplicación porque
no se modifican valores. El bloqueo solo debe presentarse cuando
un hilo esté escribiendo sobre ese archivo debido a que si se
intenta leer mientras está escribiendo se obtendrán resultados
inconsistentes.

Para eliminar el problema, cuando un hilo necesita acceder a
un recurso para actualizar sus valores, debe obtener un bloqueo
en el objeto qu representa el recurso (*WriteLock*).
También existe el bloqueo de lectura (*ReadBlock*) pero este
solo funciona para que otros hilos no intenten acceder 
a escribir el archivo.

## Ejercicio propuesto
Consiste en un programa que simula la verificación (*checkout*),
ingreso o retiro de elementos bibliotecarios. La idea es que
mientras que un elemento de la biblioteca se encuentre dentro, este
podrá ser consultado por cuantos usuarios quieran (*Parece que es
un libro de ciencias de la computación I*) pero solo podrá
ser prestado a un miembro a la vez.
El patrón es utilizado cuando el libro tiene un estado de *"StatusCheck"*
en el que adquiere un bloqueo de lectura, por lo que el siguiente miembro
puede seguir consultando, sin embargo cuando el recurso es prestado
(*"CheckOut"*) este adquiere un bloqueo de escritura, por lo que ya
no puede ser consultado, sino hasta que es devuelto, y se notifica a los
hilos de la disponibilidad del recurso.

![modelo funcional](./img/modelo_funcional.png 'modelo funcional')
![modelo funcional](./img/modelo_estructural.png 'modelo estructural')